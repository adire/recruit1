@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

@if(Session::has('nointerviews'))
<div class = 'alert alert-danger'>
    {{Session::get('nointerviews')}}
</div>
@endif

<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table">
    <tr>
        <th>id</th><th>Interview date</th><th>Summary</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date_interview}}</td>
            <td>{{$interview->summary}}</td>                           
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
                                                                           
        </tr>
    @endforeach
</table>
@endsection

