@extends('layouts.app')

@section('title', 'Edit candidate')

@section('content')       
       <h1>Edit Candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">Candidate name</label>
            <input type = "text" class="form-control" style="width: 25%" name = "name" value = {{$candidate->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">Candidate email</label>
            <input type = "text" class="form-control" style="width: 25%" name = "email" value = {{$candidate->email}}>
        </div> 
        <div>
            <input type = "submit"  name = "submit" value = "Update Candidate" class="btn btn-primary">
        </div>                       
        </form>    
    </body>
</html>
@endsection
