<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'summary' => 'good interview',
                'date_interview' => '2020-06-01',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'summary' => 'bad interview',
                'date_interview' => '2020-07-01',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'summary' => 'bad interview',
                'date_interview' => '2020-07-10',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'summary' => 'good interview',
                'date_interview' => '2020-07-15',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                  
            ]); 
    }
}
