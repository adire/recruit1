@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create Candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Candidate name</label>
            <input type = "text" class="form-control" style="width: 25%" name = "name">
        </div>     
        <div class="form-group">
            <label for = "email">Candidate email</label>
            <input type = "text" class="form-control" style="width: 25%" name = "email">
        </div> 
        <div>
            <input type = "submit"  name = "submit" value = "Create candidate" class="btn btn-primary">
        </div>                       
        </form>    
@endsection
